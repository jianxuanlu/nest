git pull
project='nest'
docker stop $project
docker rm $project
docker rmi $project
docker build -t $project .
docker run --name=$project -d -p 5000:3000  --mount type=bind,src=/data,dst=/data $project
