import content from './package.json';
export type Script = keyof typeof content.scripts;
export const script = process.env.npm_lifecycle_event as Script;
