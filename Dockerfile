FROM node:alpine
RUN node -v
# ENV PROJECT_ENV production
# ENV NODE_ENV production

WORKDIR /code

ADD package.json /code
RUN npm config set registry https://r.npm.taobao.org/
RUN npm install
ADD . /code
RUN npm run build
RUN mkdir /data
CMD [ "node", "dist/src/main.js" ]