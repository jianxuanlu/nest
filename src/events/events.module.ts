import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Message, MessageSchema } from '../message/scheme/message.scheme';
import { EventsGateway } from './events.gateway';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Message.name, schema: MessageSchema }]),
  ],
  exports: [EventsGateway],
  providers: [EventsGateway],
})
export class EventsModule {}
