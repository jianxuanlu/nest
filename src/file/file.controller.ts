import {
  Controller,
  Post,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { FileService } from './file.service';
import { Files } from './file';
import { FileFieldsInterceptor } from '@nestjs/platform-express';

@Controller('api')
export class FileController {
  constructor(private readonly fileService: FileService) {}
  @Post('file')
  @UseInterceptors(
    FileFieldsInterceptor([{ name: 'file' }, { name: 'compressedFile' }]),
  )
  saveFile(@UploadedFiles() files: Files) {
    const res = this.fileService.saveFile(files);
    return res;
  }
}
