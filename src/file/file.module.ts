import { Module } from '@nestjs/common';
import { EventsModule } from '../events/events.module';
import { FileController } from './file.controller';
import { FileService } from './file.service';

@Module({
  imports: [EventsModule],
  controllers: [FileController],
  providers: [FileService],
})
export class FileModule {}
