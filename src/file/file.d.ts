import { messageType } from 'src/message/scheme/message.scheme';

export type File = {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  buffer: NodeJS.ArrayBufferView;
};
export type Body = {
  nickName: string;
  date: string;
  socketId: string;
  type: messageType;
};
export type Files = {
  file: File[];
  compressedFile: File[];
};
