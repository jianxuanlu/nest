import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type MessageDocument = Message & Document;
export type messageType = 'text' | 'audio' | 'image' | 'video';
export class MessageWithOutId {
  @Prop()
  type: messageType;
  @Prop({ required: false })
  fileName: string;
  @Prop()
  value: string;
  @Prop()
  nickName: string;
  @Prop()
  date: string;
}
@Schema()
export class Message extends MessageWithOutId {
  _id: string;
}

export const MessageSchema = SchemaFactory.createForClass(Message);
