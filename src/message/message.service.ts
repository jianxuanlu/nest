import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Message, MessageDocument } from './scheme/message.scheme';

@Injectable()
export class MessageService {
  constructor(
    @InjectModel(Message.name) private readonly message: Model<MessageDocument>,
  ) {}
  async getMessage() {
    return await this.message.find();
  }
}
