import { Controller, Get } from '@nestjs/common';
import { MessageService } from './message.service';

@Controller('api')
export class MessageController {
  constructor(private readonly messageService: MessageService) {}
  @Get('message')
  async getMessage() {
    return await this.messageService.getMessage();
  }
}
