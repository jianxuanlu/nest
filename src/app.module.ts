import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EventsModule } from './events/events.module';
import { MessageModule } from './message/message.module';
import { FileModule } from './file/file.module';
import { ip } from '../config';

@Module({
  imports: [
    MongooseModule.forRoot(`mongodb://${ip}:27018/xxx`),
    EventsModule,
    MessageModule,
    FileModule,
  ],
})
export class AppModule {}
